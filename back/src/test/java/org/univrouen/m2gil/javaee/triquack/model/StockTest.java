package org.univrouen.m2gil.javaee.triquack.model;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StockTest {
  @Test
  public void testWithStockOfStringsIs0AfterConstruction() {
    Stock<String> myStrings = new Stock<>();
    assertThat(myStrings.getQuantity()).isEqualTo(0);
  }
  
  @Test
  public void testWithStockOfStringsIsSetWell() {
    Stock<String> myStrings = new Stock<>();
    final int q = 3;
    myStrings.setQuantity(q);
    assertThat(myStrings.getQuantity()).isEqualTo(q);  
  }
  
  @Test(expected = IllegalArgumentException.class)
  public void testWithStockOfStringsAddNegativeQuantity() {
    Stock<String> myStrings = new Stock<>();
    final int q = -3;
    myStrings.add(q);
  }
  
  @Test(expected = IllegalArgumentException.class)
  public void testWithStockOfStringsAddZeroQuantity() {
    Stock<String> myStrings = new Stock<>();
    final int q = 0;
    myStrings.add(q);
  }
  
  @Test(expected = IllegalArgumentException.class)
  public void testWithStockOfStringsRemoveNegativeQuantity() {
    Stock<String> myStrings = new Stock<>();
    final int q = -3;
    myStrings.add(q);
  }
  
  @Test(expected = IllegalArgumentException.class)
  public void testWithStockOfStringsRemoveZeroQuantity() {
    Stock<String> myStrings = new Stock<>();
    final int q = 0;
    myStrings.add(q);
  }
  
  @Test
  public void testWithStockOfStringsAddGoodQuantity() {
    Stock<String> myStrings = new Stock<>();
    final int q = 3;
    myStrings.add(q);
    assertThat(myStrings.getQuantity()).isEqualTo(3);
  }
  
  @Test
  public void testWithStockOfStringsIsAvailable() {
    Stock<String> myStrings = new Stock<>();
    final int q = 3;
    assertThat(myStrings.isAvailable()).isFalse();
    myStrings.add(q);
    assertThat(myStrings.isAvailable()).isTrue();
  }
}
