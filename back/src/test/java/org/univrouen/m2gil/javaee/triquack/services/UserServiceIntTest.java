// These are integration tests for user webservice.
package org.univrouen.m2gil.javaee.triquack.services;

import org.junit.*;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.*;
import org.springframework.boot.test.autoconfigure.web.servlet.*;
import org.springframework.boot.test.autoconfigure.orm.jpa.*;
import org.springframework.boot.test.mock.mockito.*;
import org.springframework.boot.test.web.client.*;

import org.springframework.test.web.servlet.*;
import org.springframework.test.context.junit4.SpringRunner;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.assertj.core.api.Assertions.*;

import static org.mockito.BDDMockito.*;

import java.util.*;

import org.univrouen.m2gil.javaee.triquack.dao.UserRepository;
import org.univrouen.m2gil.javaee.triquack.model.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserServiceIntTest {  
  @Autowired
  private MockMvc client;
  
  @Autowired
  private UserRepository userRepository;
  
  private String baseUri = "/triquack/api/v1/user";
  
  @Test
  public void testGetUsersWithEmptyDb() throws Exception {
    client.perform(get(baseUri + "/users"))
      .andExpect(status().isOk())
      .andExpect(content().string("[]"));
  }
  
  @Test
  public void testGetUsersWithNonEmptyDb() throws Exception {
    User u = new User();
    u.setEmail("un@email.fr");
    u.setEncodedPassword("verysecure");
    u.setRole(Role.USER);
    User repoUser = userRepository.save(u);
    client.perform(get(baseUri + "/users"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$").isNotEmpty())
      .andExpect(jsonPath("$", hasSize(1)))
      .andExpect(jsonPath("$[0].id").value(u.getId()))
      .andExpect(jsonPath("$[0].email").value(u.getEmail()))
      .andExpect(jsonPath("$[0].encodedPassword").value(u.getEncodedPassword()))
      .andExpect(jsonPath("$[0].role").value(u.getRole().toString()))
    ;
  }
  
  @Test
  public void testGetUserByIdWithNonExistingUser() throws Exception {
    client.perform(get(baseUri + "/find/id/1"))
      .andExpect(status().isNotFound())
      .andExpect(jsonPath("$").doesNotExist())
    ;
  }
  
  @Test
  public void testGetUserByIdWithExistingUser() throws Exception {
    User u = new User();
    u.setEmail("un@email.fr");
    u.setEncodedPassword("verysecure");
    u.setRole(Role.USER);
    User repoUser = userRepository.save(u);
    client.perform(get(baseUri + "/find/id/" + repoUser.getId()))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$").isNotEmpty())
      .andExpect(jsonPath("$.email").value(u.getEmail()))
      .andExpect(jsonPath("$.encodedPassword").value(u.getEncodedPassword()))
      .andExpect(jsonPath("$.role").value(u.getRole().toString()))
    ;
  }
  
  @After
  public void cleanDb() {
    userRepository.deleteAll();
  }
}
