package org.univrouen.m2gil.javaee.triquack.dao;

import org.springframework.data.repository.CrudRepository;

import javax.transaction.*;

import org.univrouen.m2gil.javaee.triquack.model.*;

@Transactional
public interface LoanableObjectRepository
                 extends CrudRepository<LoanableObject, Long> {
}
