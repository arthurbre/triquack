package org.univrouen.m2gil.javaee.triquack.model;

import javax.persistence.*;
import javax.validation.*;
import javax.validation.constraints.*;
import java.util.*;
import java.time.ZonedDateTime;

@Entity
public class Loan {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    
    @OneToOne
    @Valid
    private User user;
    
    @OneToMany
    @Valid
    @NotNull
    private Collection<LoanableObject> object;
    
    @NotNull
    private ZonedDateTime startDate;
    
    @DecimalMin("1")
    private int duration;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void addObject(LoanableObject loanableObject) {
        if (loanableObject == null) {
          throw new IllegalArgumentException();
        }
        object.add(loanableObject);
    }

    public void removeObject(LoanableObject loanableObject) {
        object.remove(loanableObject);
    }

    public void removeObject(long loanableObject) {
        for (LoanableObject lo : object) {
            if (lo.getId() == loanableObject) {
                object.remove(lo);
            }
        }
    }
    
    public Collection<LoanableObject> getObjects() {
      return Collections.unmodifiableCollection(object);
    }
    
    public ZonedDateTime getStartDate() {
      return startDate;
    }
    
    public void setStartDate(ZonedDateTime zdt) {
      startDate = zdt;
    }
    
    public int getDuration() {
      return duration;
    }
}
