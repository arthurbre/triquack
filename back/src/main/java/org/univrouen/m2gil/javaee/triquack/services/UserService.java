package org.univrouen.m2gil.javaee.triquack.services;

import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.validation.annotation.Validated;

import javax.validation.*;
import javax.validation.constraints.*;
import java.lang.Iterable;
import java.util.*;
import java.net.*;

import org.univrouen.m2gil.javaee.triquack.dao.UserRepository;
import org.univrouen.m2gil.javaee.triquack.model.User;

@RestController
@RequestMapping("/triquack/api/v1/user")
public class UserService {
  @Autowired
  private UserRepository repo;
  
  @GetMapping("/users")
  public ResponseEntity<Iterable<User>> getUsers() {
    return ResponseEntity.ok(repo.findAll());
  }
  
  @GetMapping("/find/id/{id}")
  public ResponseEntity<User>
  getUserById(@PathVariable("id") long id) {
    Optional<User> u = repo.findById(id);
    if (u.isPresent()) {
      return ResponseEntity.ok(u.get());
    } else {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
  }
  
  @GetMapping("/find/email")
  public ResponseEntity<User>
  getUserByEmail(@RequestParam(value = "email", required = true) @Email String email) {
    Optional<User> u = repo.findByEmail(email);
    if (u.isPresent()) {
      return ResponseEntity.ok(u.get());
    } else {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
  }
  
  @PostMapping("/create")
  public ResponseEntity<User> 
  addUser(@Validated @RequestBody User user) 
  throws URISyntaxException {
    User repoUser = repo.save(user);
    return ResponseEntity.created(new URI("/find/id/" + repoUser.getId())).build();
  }
}
