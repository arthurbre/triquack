package org.univrouen.m2gil.javaee.triquack.model;

public enum Role {
  USER, ADMIN;
}
