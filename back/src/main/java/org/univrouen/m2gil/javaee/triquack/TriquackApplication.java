package org.univrouen.m2gil.javaee.triquack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TriquackApplication {

  public static void main(String[] args) {
    SpringApplication.run(TriquackApplication.class, args);
  }
}
