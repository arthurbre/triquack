package org.univrouen.m2gil.javaee.triquack.model;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.univrouen.m2gil.javaee.triquack.model.util.Constants;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    
    @Email
    @NotNull
    @Size(max = Constants.MAX_STRING_LENGTH)
    private String email;
    
    @NotNull
    @Size(max = Constants.MAX_STRING_LENGTH)
    private String encodedPassword;
    
    @Enumerated(EnumType.STRING)
    @NotNull
    private Role role;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEncodedPassword() {
        return encodedPassword;
    }
    
    public void setEncodedPassword(String encodedPassword) {
        this.encodedPassword = encodedPassword;
    }
    
    public Role getRole() {
      return role;
    }
    
    public void setRole(Role role) {
      this.role = role;
    }
}
