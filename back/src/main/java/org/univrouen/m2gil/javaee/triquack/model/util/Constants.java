package org.univrouen.m2gil.javaee.triquack.model.util;

public final class Constants {
  public static final int MAX_STRING_LENGTH = 255;
  
  private Constants() {}
}
