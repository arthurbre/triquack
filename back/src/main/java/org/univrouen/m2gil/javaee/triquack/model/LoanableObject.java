package org.univrouen.m2gil.javaee.triquack.model;

import javax.persistence.*;
import javax.validation.constraints.*;

import org.univrouen.m2gil.javaee.triquack.model.util.Constants;

@Entity
public abstract class LoanableObject {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private long id;
    
    @Size(max = Constants.MAX_STRING_LENGTH)
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
