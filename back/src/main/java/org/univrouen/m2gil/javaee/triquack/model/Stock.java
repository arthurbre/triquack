package org.univrouen.m2gil.javaee.triquack.model;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
public class Stock<E> {
    @DecimalMin("0")
    private int quantity;
    
    @Id
    private long id;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isAvailable() {
        return !( quantity == 0);
    }

    public void add(int qty) {
      if (qty <= 0) {
        throw new IllegalArgumentException();
      } else {
        quantity += qty;
      }
    }

    public void remove(int qty) {
      if (qty <= 0) {
        throw new IllegalArgumentException();
      } else {  
        quantity = (int) Math.max(0.0, quantity - qty);
      }
    }
}
