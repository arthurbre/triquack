package org.univrouen.m2gil.javaee.triquack.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.*;
import java.util.*;

import org.univrouen.m2gil.javaee.triquack.model.*;

@Transactional
public interface UserRepository
                 extends CrudRepository<User, Long> {
  @Query("SELECT u FROM User u WHERE u.email LIKE ?1")
  Optional<User> findByEmail(String email);
}
